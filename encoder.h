/*
 * encoder.h
 *
 * Created: 2021-10-10 12:38:31
 *  Author: Anton
 */ 


#ifndef ENCODER_H_
#define ENCODER_H_

#include <stdint.h>

struct EncoderData {
	int currentState;
	int8_t updated;
};

struct DebounceData {
	int count;
	int threshold;
	int state;
	int prevState;
	int latch;
	int updated;
};

void InitEncoder(struct EncoderData *enc);
void EncoderTask(struct EncoderData *enc, int a, int b);

void InitDebounce(struct DebounceData *sw, int initialPinValue, int threshold);
void DebounceTask(struct DebounceData *sw, int sw_input);




#endif /* ENCODER_H_ */