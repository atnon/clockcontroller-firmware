/*
             LUFA Library
     Copyright (C) Dean Camera, 2017.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2017  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

/** \file
 *
 *  Main source file for the VirtualSerial demo. This file contains the main tasks of
 *  the demo and is responsible for the initial application hardware configuration.
 */

#include "ClockController.h"

/*
 PD5    Debug LED   Output   Active High
 
 PB4    Encoder Sw  Input   Active High
 PB5    Encoder A   Input
 PB6    Encoder B   Input
 
 RTC on SPI, using Chip Enable on SS/PB0
 PD2    RTC 1Hz     Input   Active Low?
 PC6    RTC Int0    Input   Active High
 PC7    RTC Int1    Input   Active Low
 */

/** LUFA CDC Class driver interface configuration and state information. This structure is
 *  passed to all CDC Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface =
	{
		.Config =
			{
				.ControlInterfaceNumber   = INTERFACE_ID_CDC_CCI,
				.DataINEndpoint           =
					{
						.Address          = CDC_TX_EPADDR,
						.Size             = CDC_TXRX_EPSIZE,
						.Banks            = 1,
					},
				.DataOUTEndpoint =
					{
						.Address          = CDC_RX_EPADDR,
						.Size             = CDC_TXRX_EPSIZE,
						.Banks            = 1,
					},
				.NotificationEndpoint =
					{
						.Address          = CDC_NOTIFICATION_EPADDR,
						.Size             = CDC_NOTIFICATION_EPSIZE,
						.Banks            = 1,
					},
			},
	};
	

/** Standard file stream for the CDC interface when set up, so that the virtual CDC COM port can be
 *  used like any regular character stream in the C APIs.
 */
static FILE USBSerialStream;








/** Main program entry point. This routine contains the overall program flow, including initial
 *  setup of all components and the main program loop.
 */

//PS C:\Users\Anton\Documents\Atmel Studio\7.0\ClockController_LUFAex\ClockController_LUFAex\Debug> C:\Users\Anton\on_path_helpers\avrdude.exe -c avrisp -p m32u4 -P COM3 -b 19200 -U flash:w:ClockController_LUFAex.hex

enum Mode CurrentMode = Idle;
enum Mode NextMode = ClockHours;
uint8_t CurrentSymbols[] = {0,0,0,0,0};
uint8_t segmentBg[5] = {0,0,0,0,0};
int UpdateQueued = false;
uint32_t counter = 0;
int SendToDisplay = false;
int BgUpdate = false;

int main(void)
{
	SetupHardware();

	/* Create a regular character stream for the interface so that it can be used with the stdio.h functions */
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);

	GlobalInterruptEnable();
	
	struct EncoderData enc;
	InitEncoder(&enc);
	
	struct DebounceData sw;
	InitDebounce(&sw, 0, 200); /* Initial state of button is 0/NO. */
	
	cmdItem cmdList[12] = {
		{"seconds", CmdSeconds, "Seconds counting mode"},
		{"freerun", CmdFreerun, "Freerunning counting mode"},
		{"idle", CmdIdle, "Idle state"},
		{"+", CmdPlus, "Increase counter"},
		{"-", CmdMinus, "Decrease counter"},
		{"cntget", CmdCntGet, "Print counter value"},
		{"segset", CmdSegset, "Set segment numerical values. Comma separated."},
		{"timeset", CmdTimeset, "Set time timeset h,m,s"},
		{"minutes", CmdMinutes, "Show minutes + seconds"},
		{"hours", CmdHours, "Show hours + minutes"},
		{"sethours", CmdSetHours, "Set hours mode"},
		{"setminutes", CmdSetMinutes, "Set minutes mode"}
	};

	SetCmdList(cmdList, 12);
	
	fputs("ClockController initialization done.", &USBSerialStream);
	
	for (;;)
	{

		CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
		USB_USBTask();
		ComTask(&VirtualSerial_CDC_Interface, &USBSerialStream);
		
		int enc_a = (PINB >> PINB5) & 1;
		int enc_b = (PINB >> PINB6) & 1;
		
		EncoderTask(&enc, enc_a, enc_b);
		
		if (enc.updated) {
			if (CurrentMode == SetHours) {
				int val = CurrentSymbols[4]*10 + CurrentSymbols[3];
				if ((val == 0) && (enc.updated == -1)) {
					val = 23;
				} else {
					val = (val + enc.updated) % 24;
				}
				
				val = Bin2BDC(val);
				CurrentSymbols[4] = (val >> 4);
				CurrentSymbols[3] = (val & 0x0F);
				UpdateQueued = true;
			} else if (CurrentMode == SetMinutes) {
				int val = CurrentSymbols[1]*10 + CurrentSymbols[0];
				if ((val == 0) && (enc.updated == -1)) {
					val = 59;
				} else {
					val = (val + enc.updated) % 60;
				}
				
				val = Bin2BDC(val);
				CurrentSymbols[1] = (val >> 4);
				CurrentSymbols[0] = (val & 0x0F);
				UpdateQueued = true;
			}
			enc.updated = 0;
		}
		
		int enc_sw = (PINB >> ENC_SW) & 1;
		DebounceTask(&sw, enc_sw);
		
		if (sw.updated && sw.state) {
			//fputs("Switch changed\r\n", &USBSerialStream);
			switch(CurrentMode) {
				case ClockHours:
					NextMode = SetHours;
					break;
				case SetHours:
					NextMode = SetMinutes;
					break;
				case SetMinutes:
					NextMode = SetTime;
					break;
				default:
					break;
			}
			sw.updated = 0;
		}
		
		/* Mode transition actions. */
		if (NextMode != CurrentMode) {
			
			for (int segment = 0; segment < 5; segment++) {
				segmentBg[segment] = 0xFF; /* Clear background state, all on per default. */
			}
			switch(NextMode) {
				case SetHours:
				case SetMinutes:
					for (int segment = 0; segment < 5; segment++) {
						segmentBg[segment] = 0;
					}
					UpdateQueued = true;
					break;
				case SetTime: ;
					int h = CurrentSymbols[4]*10 + CurrentSymbols[3];
					int m = CurrentSymbols[1]*10 + CurrentSymbols[0];
					SetDS1306Time(h, m, 0); /* This is where we go ahead and set the time to the RTC. */
					NextMode = ClockHours;
					break;
				case Freerun:
					for (int segment = 0; segment < 5; segment++) {
						segmentBg[segment] = 0xFF;
						CurrentSymbols[segment] = 0;
					}
					UpdateQueued = true;
					break;
				case ClockMinutes:
				case ClockHours:
					CurrentSymbols[2] = -1; // Blank out mid character just in case.
					UpdateQueued = true;
					break;
				default:
					for (int segment = 0; segment < 5; segment++) {
						segmentBg[segment] = 0xFF;
						CurrentSymbols[segment] = 0;
					}
					break;
			}
			CurrentMode = NextMode;
		}
		
		if (BgUpdate) {
			switch(CurrentMode) {
				case SetHours:
					segmentBg[4] ^= 0xFF;
					segmentBg[3] ^= 0xFF;
					break;
				case SetMinutes:
					segmentBg[1] ^= 0xFF;
					segmentBg[0] ^= 0xFF;
					break;
				case ClockMinutes:
					/* Fall-through. */
				case ClockHours:
					segmentBg[2] ^= 0xFF;
					break;
				default:
					break;
			}
			BgUpdate = false;
			SendToDisplay = true;
		}
				
		if (UpdateQueued) {
			switch(CurrentMode) {
				
				case Freerun:
					counter++;
					CountToSymbols();
					UpdateQueued = true;
					break;
				case Seconds:
					counter++;
					CountToSymbols();
					UpdateQueued = false;
					break;
				case ClockMinutes:
					GetDS1306Time(false);
					UpdateQueued = false;
					break;
				case ClockHours:
					GetDS1306Time(true);
					UpdateQueued = false;
					break;
				default:
					UpdateQueued = false; /* Clear UpdateQueued if not handled elsewhere. */
					break;
			}
			SendToDisplay = true;
			
			
		}
		
		if (SendToDisplay) {
			for (int segment = 0; segment < 5; segment++) {
				if (CurrentSymbols[segment] != -1) {
					SetLedState(segment,  CurrentSymbols[segment], 0xFF, segmentBg[segment]);
					} else {
					SetLedState(segment,  CurrentSymbols[segment], 0x00, segmentBg[segment]);
				}
			}
			SendToDisplay = false;
		}
			
		if (counter > 99999) {
			counter = 0;
		}
	}
}

void SetDS1306Time(int h, int m, int s) {
	fputs("DS1306->set", &USBSerialStream);
	PORTB |= (1 << PB0); /* CE active */
	SPI_SendByte(0x80); /* Address */
	SPI_SendByte(Bin2BDC(s)); /* Write seconds. */
	SPI_SendByte(Bin2BDC(m)); /* Write minutes. Address is auto-incremented. */
	SPI_SendByte(Bin2BDC(h)); /* Write hours. Address is auto-incremented. */
	PORTB &= ~(1 << PB0);
}

void GetDS1306Time(int showHours) {
	PORTB |= (1 << PB0); /* CE active */
	SPI_SendByte(0x00); /* Address - read seconds*/
	uint8_t secondsBCD = SPI_ReceiveByte();
	uint8_t minutesBCD = SPI_ReceiveByte();
	uint8_t hoursBCD = SPI_ReceiveByte();
	PORTB &= ~(1 << PB0);
	
	if (showHours) {
		
		CurrentSymbols[0]=	minutesBCD & 0x0F;
		CurrentSymbols[1] = minutesBCD >> 4;
		CurrentSymbols[3] = hoursBCD & 0x0F;
		CurrentSymbols[4] = hoursBCD >> 4;
	} else {
		CurrentSymbols[0] = secondsBCD & 0x0F;
		CurrentSymbols[1] = secondsBCD >> 4;
		CurrentSymbols[3]=	minutesBCD & 0x0F;
		CurrentSymbols[4] = minutesBCD >> 4;
	}
	
	UpdateQueued = true;
}

void CmdSeconds(char **saveptr) {NextMode = Seconds;}
void CmdFreerun(char **saveptr) {NextMode = Freerun;}
void CmdIdle(char **saveptr) {NextMode = Idle;}
void CmdPlus(char **saveptr) {
	counter++;
	CountToSymbols();
	UpdateQueued = true;
}
void CmdMinus(char **saveptr) {
	counter--;
	CountToSymbols();
	UpdateQueued = true;
}
void CmdCntGet(char **saveptr) {fprintf(&USBSerialStream, "%i\r\n", counter);}
void CmdSegset(char **saveptr) {
	fputs("segset\r\n", &USBSerialStream);
	for (int i = 0; i < 5; i++)
	{
		char *token = strtok_r(NULL, ",", saveptr);
		if (token != NULL)
		{
			int val = atoi(token);
			CurrentSymbols[i] = val;
		}
		else
		{
			fputs("Too few arguments.\r\n", &USBSerialStream);
			break;
		}
	}
	UpdateQueued = true;
}
void CmdTimeset(char **saveptr) {
	fputs("timeset\r\n", &USBSerialStream);
	int hms[3];
	for (int i = 0; i < 3; i++)
	{
		char *token = strtok_r(NULL, ",", saveptr);
		if (token != NULL)
		{
			int val = atoi(token);
			hms[i] = val;
		}
		else
		{
			fputs("Too few arguments.\r\n", &USBSerialStream);
			break;
		}
	}
	//fputs("all proc\r\n", &USBSerialStream);
	fprintf(&USBSerialStream, "%i, %i, %i\r\n", hms[0], hms[1], hms[2]);
	SetDS1306Time(hms[0], hms[1], hms[2]);
}
void CmdMinutes(char **saveptr) {NextMode = ClockMinutes;}
void CmdHours(char **saveptr) {NextMode = ClockHours;}
void CmdSetHours(char **saveptr) {NextMode = SetHours;}
void CmdSetMinutes(char **saveptr) {NextMode = SetMinutes;}



int Bin2BDC(int binval) {
	uint8_t upper = binval / 10;
	uint8_t lower = binval % 10;
	return (upper << 4) | lower;
}

void CountToSymbols(void) {
	CurrentSymbols[0] = counter % 10;
	CurrentSymbols[1] = (int)(floor(counter / 10)) % 10;
	CurrentSymbols[2] = (int)(floor(counter / 100)) % 10;
	CurrentSymbols[3] = (int)(floor(counter / 1000)) % 10;
	CurrentSymbols[4] = (int)(floor(counter / 10000)) % 10;
}

ISR(INT2_vect) {
	switch(CurrentMode) {
		case Seconds:
		case ClockHours:
		case ClockMinutes:
			UpdateQueued = true;
			BgUpdate = true;
		break;
		case SetMinutes:
		case SetHours:
			BgUpdate = true;
	default:
		break;
	}
}




/** Each segment TLC59116 has 13 LEDs attached, where the PWM0-9 are mapped to digits 0-9, decimal dot to PWM10 red background to PWM11 and LED_STATUS (backside) to PWM15  */
void SetLedState(int SegmentIndex, int num, int state, int bg) {
	uint8_t BusAddress = TLC_BASE_ADDR + (SegmentIndex << 1);
	uint8_t InternalWriteAddress =	TLC_PWM0_REG | TLC_AUTO_INCREMENT; /* Setup for the first PWM register and enable auto incrementation. */
	uint8_t WritePacket[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	WritePacket[num] = state; /* 0xFF = Full power. */
	WritePacket[11] = bg;
	
	TWI_WritePacket(
		BusAddress | TLC_WRITE, 10, /* Address and timeout (ms). Shifting is needed due to the lowest bit denoting read/write. */
		&InternalWriteAddress, sizeof(InternalWriteAddress), /* Register address */
		&WritePacket, sizeof(WritePacket) /* Register data */
	);
}

/* 
Setup a display segments TLC59116 to enable its internal oscillator and do auto-incrementation for register accesses. 
Segments are addressed 0-4 right to left.
*/
void SetupTLC59116(int SegmentIndex) {
	uint8_t BusAddress = TLC_BASE_ADDR + (SegmentIndex << 1);
	
	/* Enable the thing. */
	uint8_t InternalWriteAddress =	TLC_MODE1_REG; /* MODE1 register */
	const uint8_t WritePacketMode1 =	TLC_MODE1_AI2; /* Auto increment, OSC will implicitly be set active (=0). */
	TWI_WritePacket(
		BusAddress | TLC_WRITE, 10, /* Address and timeout (ms). Shifting is needed due to the lowest bit denoting read/write. */
		&InternalWriteAddress, sizeof(InternalWriteAddress), /* Register address */
		&WritePacketMode1, sizeof(WritePacketMode1) /* Register data */
	);
	
	/* Set all LEDs to be PWM controlled. */
	InternalWriteAddress =	TLC_LEDOUT0_REG | TLC_AUTO_INCREMENT;
	const uint8_t WritePacketLedOut[4] = {0b10101010, 0b10101010, 0b10101010, 0b10101010}; /* Each LED takes two bits, PWM control is 0b10. Repeat for all. */
	TWI_WritePacket(
		BusAddress | TLC_WRITE, 10, /* Address and timeout (ms). Shifting is needed due to the lowest bit denoting read/write. */
		&InternalWriteAddress, sizeof(InternalWriteAddress), /* Register address */
		&WritePacketLedOut, sizeof(WritePacketLedOut) /* Register data */
	);
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void SetupHardware(void)
{
	/* Disable watchdog if enabled by bootloader/fuses */
	MCUSR &= ~(1 << WDRF);
	wdt_disable();

	/* Disable clock division */
	clock_prescale_set(clock_div_1);
	
	/* SPI Init. */
	SPI_Init(SPI_SPEED_FCPU_DIV_16 | SPI_ORDER_MSB_FIRST | SPI_SCK_LEAD_FALLING | SPI_SAMPLE_TRAILING | SPI_MODE_MASTER);

	/* Hardware Initialization */
	USB_Init();
	//SPI_Init(SPI_SPEED_FCPU_DIV_16 | SPI_ORDER_MSB_FIRST | SPI_SCK_LEAD_FALLING | SPI_SAMPLE_TRAILING | SPI_MODE_MASTER);
	TWI_Init(TWI_BIT_PRESCALE_1, TWI_BITLENGTH_FROM_FREQ(1, 200000));
	
	DDRB |= (1<<PB0) | (1<<PB1) | (1<<PB2); /* Set SS, SCLK and MOSI as outputs. */
	DDRD |= (1<<PD5);   /* Debug LED set as output. */
	
	/* Initialize LED Drivers. */
	for (int segment = 0; segment < 5; segment++) {
		SetupTLC59116(segment);
	}
	
	/* Setup DS1306 to output 1 Hz signal connected to PD2/INT2. */
	
	PORTB |= (1 << PB0);
	SPI_SendByte(0x8F);
	SPI_SendByte((1<<2));
	PORTB &= ~(1 << PB0);
	
	/* Setup rising-edge interrupt for PD2/INT2. */
	EICRA |= (1 << ISC20) | (1 << ISC21); // Rising edge.
	EIMSK |= (1 << INT2); // Enable interrupt generation for INT2.
}


/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
	
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
	
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	ConfigSuccess &= CDC_Device_ConfigureEndpoints(&VirtualSerial_CDC_Interface);
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
	CDC_Device_ProcessControlRequest(&VirtualSerial_CDC_Interface);
}

/** CDC class driver callback function the processing of changes to the virtual
 *  control lines sent from the host..
 *
 *  \param[in] CDCInterfaceInfo  Pointer to the CDC class interface configuration structure being referenced
 */
void EVENT_CDC_Device_ControLineStateChanged(USB_ClassInfo_CDC_Device_t *const CDCInterfaceInfo)
{
	/* You can get changes to the virtual CDC lines in this callback; a common
	   use-case is to use the Data Terminal Ready (DTR) flag to enable and
	   disable CDC communications in your application when set to avoid the
	   application blocking while waiting for a host to become ready and read
	   in the pending data from the USB endpoints.
	*/
	bool HostReady = (CDCInterfaceInfo->State.ControlLineStates.HostToDevice & CDC_CONTROL_LINE_OUT_DTR) != 0;
}
