/*
 * encoder.c
 *
 * Created: 2021-10-10 12:35:34
 *  Author: Anton
 */ 

#include "encoder.h"

void InitEncoder(struct EncoderData *enc) {
	enc->currentState = 0b11;
	enc->updated = 0;
}

void EncoderTask(struct EncoderData *enc, int a, int b) {
	
	/* Encoder state machine */
	int dataIn = ((b << 1) | a );
	
	switch(enc->currentState) {
		case 0b11: /* Encoder is in a default detent. */
			/* Always starting here, more or less. */
			if (dataIn == 0b00) {
				enc->currentState = 0b00;
			}
			break;
		case 0b00:
			if (dataIn == 0b01) {
				enc->currentState = 0b01;
			} else if (dataIn == 0b10) {
				enc->currentState = 0b10;
			}
			break;
		case 0b01:
			if (dataIn == 0b11) {
				enc->currentState = 0b11;
				enc->updated = 1; /* Clockwise = increment. */
			} else if (dataIn == 0b00) {
				enc->currentState = 0b00;
			}
			break;
		case 0b10:
			if (dataIn == 0b11) {
				enc->currentState = 0b11;
				enc->updated = -1; /* Counter-clockwise = decrement. */
			} else if (dataIn == 0b00) {
				enc->currentState = 0b00;
			}
			break;
		default:
			break;
	}
	
	
}

void InitDebounce(struct DebounceData *sw, int initialPinValue, int threshold) {
	sw->count = 0;
	sw->latch = 0;
	sw->prevState = initialPinValue;
	sw->state = initialPinValue;
	sw->threshold = threshold;
	sw->updated = 0;
}

void DebounceTask(struct DebounceData *sw, int swInput) {
	/* Switch state machine. */
	if (swInput != sw->prevState) {
		sw->prevState = swInput;
		sw->count = 0;
		sw->latch = 0;
	}
	if (sw->count > sw->threshold) {
		sw->state = sw->prevState;
		sw->latch = 1;
		sw->updated = 1;
		sw->count = 0;
	} else if (sw->latch == 0) {
		sw->count++;
	}
}