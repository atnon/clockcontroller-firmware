/*
 * CFile1.c
 *
 * Created: 2021-10-10 13:38:55
 *  Author: Anton
 */ 

#include "CmdLine.h"

uint8_t CmdBuffer[128];
uint8_t *CmdBufferPtr = CmdBuffer;

cmdItem *cmdList = NULL;
int cmdListLength = 0;


void processCmd(uint8_t *buffer, FILE *USBSerialStream) {
	
	char *saveptr;
	char *token = strtok_r(buffer, " ", &saveptr);
	
	fputs("\r\n", USBSerialStream);
	
	int success = false;
	
	cmdItem *ptr = cmdList;
	for(int i=0; i < cmdListLength; i++, ptr++) {
		
		if (strcmp(token, ptr->name) == 0) {
			ptr->action(&saveptr);
			success = true;
			break;
		}
	}
	
	// If we get this far, there was no hit amongst the commands.
	if (success == false) {
		ptr = cmdList;
		fputs("Available commands:\r\n", USBSerialStream);
		for (int i=0; i < cmdListLength; i++, ptr++) {
			fputs("\t", USBSerialStream);
			fputs(ptr->name, USBSerialStream);
			fputs("\t", USBSerialStream);
			fputs(ptr->help, USBSerialStream);
			fputs("\r\n", USBSerialStream);
		}
	}
	
	}

void ComTask(USB_ClassInfo_CDC_Device_t *interface, FILE *USBSerialStream) {
	char CharIn;
	if (CDC_Device_BytesReceived(interface)) {
		CharIn = CDC_Device_ReceiveByte(interface);
		if ((CharIn == 0x0A) || (CharIn == 0x0D)) {
			*CmdBufferPtr = NULL;
			CmdBufferPtr = CmdBuffer;
			processCmd(CmdBuffer, USBSerialStream);
		} else {
			*CmdBufferPtr = CharIn;
			fputc(CharIn, USBSerialStream);
			if (CmdBufferPtr < (CmdBuffer + sizeof(CmdBuffer))) {
				CmdBufferPtr++;
				} else {
				CmdBufferPtr = CmdBuffer;
			}
			
		}
	}
}

void SetCmdList(cmdItem *cmdDef, int numItems) {
	cmdList = cmdDef;
	cmdListLength = numItems;
}