/*
 * CmdLine.h
 *
 * Created: 2021-10-10 13:44:27
 *  Author: Anton
 */ 

#ifndef CMDLINE_H_
#define CMDLINE_H_

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <LUFA/Drivers/USB/USB.h>

typedef void (*pfnAction)(char **saveptr);

typedef struct {
	char *name;
	pfnAction action;
	char *help;
} cmdItem;

void processCmd(uint8_t *buffer, FILE *USBSerialStream);
void ComTask(USB_ClassInfo_CDC_Device_t *interface, FILE *USBSerialStream);
void SetCmdList(cmdItem *cmdDef, int numItems);

#endif /* CMDLINE_H_ */